#!/usr/bin/perl
use strict;
use warnings;
my $boss_first_name = "Penelope";
my $boss_last_name = "Creighton-Ward ";
my $butler_first_name = "Aloysius";
my $butler_last_name = "Parker";
my $field_width = 15;
if ($field_width > length($boss_first_name) && $field_width > length($butler_first_name) && $field_width > length($boss_last_name) && $field_width > length($butler_last_name))
{
	print "|$boss_first_name $boss_last_name|$butler_first_name $butler_last_name|\n"; 
}
elsif (($field_width < length($boss_first_name) || $field_width < length($boss_last_name)) && ($field_width < length($butler_first_name) || $field_width < length($butler_last_name)))
{
	if (length($boss_first_name) > length($boss_last_name))
	{
		$boss_last_name.= (' ' x (length($boss_first_name) - length($boss_last_name)));
	}
	else
	{
		$boss_first_name.=(' ' x (length($boss_last_name) - length($boss_first_name)));
	}
	
	if (length($butler_first_name) > length($butler_last_name))
	{
		$butler_last_name.= (' ' x (length($butler_first_name) - length($butler_last_name)));
	}
	else
	{
		$butler_first_name.=(' ' x (length($butler_last_name) - length($butler_first_name)));
	}
	print "|$boss_first_name|$butler_first_name|\n|$boss_last_name|$butler_last_name|\n"; 
}
elsif(($field_width <= length($boss_first_name) || $field_width <= length($boss_last_name)) && ($field_width >= length($butler_first_name) || $field_width >= length($butler_last_name)))
{
	my $space ='';
	my $space2 = '';
	if (length($boss_first_name) > length($boss_last_name))
	{
		$boss_last_name.= (' ' x (length($boss_first_name) - length($boss_last_name)));
		$space .= (' 'x (length($boss_first_name)));
		$space2 .= (' 'x (length($butler_first_name.' '.$butler_last_name)));
	}
	else
	{
		$boss_first_name.=(' ' x (length($boss_last_name) - length($boss_first_name)));
		$space .= (' 'x (length($boss_last_name)));
		$space2 .= (' 'x (length($butler_first_name.' '.$butler_last_name)));
	}
		
	print "|$space2|$space|\n";
	print "|$boss_first_name|$butler_first_name $butler_last_name|\n|$boss_last_name|$space2|"; 
}
elsif(($field_width >= length($boss_first_name) || $field_width >=length($boss_last_name)) && ($field_width <= length($butler_first_name) || $field_width <= length($butler_last_name)))
{
	my $space ='';
	my $space2 = '';
	if (length($butler_first_name) > length($butler_last_name))
	{
		$butler_last_name.= (' ' x (length($butler_first_name) - length($butler_last_name)));
		$space .= (' 'x (length($butler_first_name)));
		$space2 .= (' 'x (length($boss_first_name.' '.$boss_last_name)));
	}
	else
	{
		$butler_first_name.=(' ' x (length($butler_last_name) - length($butler_first_name)));
		$space .= (' 'x (length($butler_last_name)));
		$space2 .= (' 'x (length($boss_first_name.' '.$boss_last_name)));
	}
	
	
	print "|$space|$space|\n";
	print "|$boss_first_name $boss_last_name|$butler_first_name|\n|$space2|$butler_last_name|"; 
}